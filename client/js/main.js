var timer;
var response;
var vm = new Vue({
	el: '#yunda',
	data: {
		isStart:false,
		requestGetWorker: 0,
		requestGetCurrent: 0,
		flag: false,
		rankTitle: "特", //几等奖
		currentDraw: null,
		round: "第1", //第几轮
		column: 0,
		row: 0,
		workers: [], //总员工
		arr: [] //随机抽奖号
	},
	methods: {
		start: function() {
			vm.isStart=true;
			vm.flag = true;
			clearInterval(timer);
			timer = setInterval(function() {
				vm.enChange();
			}, 10); //随机数据变换速度，越小变换的越快
		},
		stop: function() {
			vm.isStart=false;
			clearInterval(timer);
		},
		ensure: function() {
			if(vm.isStart){
				alert("请停止后，确认进入下一轮");
				return ;
			}
			this.updateDraw();
		},
		enChange: function() {
			let workerSize = this.workers.length;
			console.log("员工数量 " + workerSize);
			this.arr = [];
			for(var i = 0; i < this.currentDraw.per;) {
				let random = parseInt(Math.random() * (workerSize));
				if($.inArray(random, this.arr) == -1) {
					this.arr[i++] = random;
				}
			}
			console.log("待抽取幸运员工编号 " + this.arr);
		},
		//初始化  标题
		initTitle: function() {

			let per = this.currentDraw.per;
			console.log("每页名额数 " + per)
			switch(this.currentDraw.rank) {
				case 1:
					this.rankTitle = "一";
					break;
				case 2:
					this.rankTitle = "二";
					break;
				case 3:
					this.rankTitle = "三";
					break;
				case 4:
					this.rankTitle = "四";
					break;
				case 0:
					this.rankTitle = "特";
					break;
			}

			let th = parseInt(this.currentDraw.currentCount / per);
			let total = parseInt(this.currentDraw.maxCount / per);
			if(th + 1 == total) {
				this.round = "最后1";
			} else {
				this.round = "第" + (th + 1);
			}
		},
		//初始化表格
		initTable: function() {
			let per = this.currentDraw.per;
			if(this.workers.length < per) {
				alert("员工数量不足，抽奖失败");
			}
			this.enChange();
			switch(per) {
				case 10:
					this.row = 5;
					this.column = 2;
					break;
				case 5:
					this.row = 5;
					this.column = 1;
					break;
				case 2:
					this.row = 1;
					this.column = 2;
					break;
				case 1:
					this.row = 1;
					this.column = 1;
			}
			
		},
		finish: function() {
			window.location.href = 'http://www.jmper.top/html/finish.html';
		},
		responseDelay: function() {
			response = setInterval(function() {
				if(vm.requestGetCurrent == 1 && vm.requestGetWorker == 0) {
					vm.initTitle();
					vm.requestGetCurrent = 2;
				}
				if(vm.requestGetWorker == 1 && vm.requestGetCurrent == 1) {
					vm.initTitle();
					vm.initTable();
					vm.requestGetCurrent = 0;
					vm.requestGetWorker = 0;
					clearInterval(response)
				}
				if(vm.requestGetWorker == 1 && vm.requestGetCurrent == 2) {
					vm.initTable();
					vm.requestGetCurrent = 0;
					vm.requestGetWorker = 0;
					clearInterval(response)
				}
			}, 50); //随机数据变换速度，越小变换的越快
		},
		getDrawCount: function() {
			$.get("http://www.jmper.top/renren-api/draw/drawCount", function(r) {

				if(r.code == 0) {
					vm.currentDraw = r.current;
					vm.requestGetCurrent = 1;
				} else {
					if(r.msg == "抽奖完毕") {
						vm.finish();
					}
					return;
				}
			})
		},
		getWorkers: function() {
			$.get("http://www.jmper.top/renren-api/draw/workers", function(r) {
				if(r.code == 0) {
					vm.workers = r.workers;
					vm.requestGetWorker = 1;
				}
			});
		},
		updateDraw: function() {
			var request = {
				numbers: [],
				rank: vm.currentDraw.rank
			}
			for(var i = 0; i < this.arr.length; i++) {
				request.numbers[i] = this.workers[this.arr[i]].number;
			}
			$.ajax({
				type: "POST",
				url: "http://www.jmper.top/renren-api/draw/updateDraw",
				contentType: "application/json",
				data: JSON.stringify(request),
				success: function(data) {
					console.log(data);
					vm.flag = false;
					vm.getDrawCount();
					vm.getWorkers();
					vm.responseDelay();
				},
				error: function() {
					alert("错误");
				}

			});
		}
	}
});

vm.getWorkers();
vm.getDrawCount();
vm.responseDelay();