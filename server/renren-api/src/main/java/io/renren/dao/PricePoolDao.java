package io.renren.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.renren.entity.PricePoolEntity;

/**
 * 奖池
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-13 22:35:17
 */
public interface PricePoolDao extends BaseMapper<PricePoolEntity> {


     PricePoolEntity selectByCurrent();

}
