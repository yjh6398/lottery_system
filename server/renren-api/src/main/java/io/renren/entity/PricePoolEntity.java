package io.renren.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.models.auth.In;

import java.io.Serializable;

/**
 * 奖池
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-04-13 22:35:17
 */
@TableName("price_pool")
public class PricePoolEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 等级  奖励
	 */
	private Integer rank;
	/**
	 * 当前 抽到此奖的人数
	 */
	private Integer currentCount;
	/**
	 * 总人数
	 */
	private Integer maxCount;
	/**
	 * 每次抽奖人数
	 */
	private Integer per;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：等级  奖励
	 */
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	/**
	 * 获取：等级  奖励
	 */
	public Integer getRank() {
		return rank;
	}

	public Integer getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(Integer currentCount) {
		this.currentCount = currentCount;
	}

	public Integer getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}
	public void setPer(Integer per) {
		this.per = per;
	}
	/**
	 * 获取：每次抽奖人数
	 */
	public Integer getPer() {
		return per;
	}
}
