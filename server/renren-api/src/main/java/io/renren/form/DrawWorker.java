package io.renren.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author 郑和明
 * @version 1.0 (createTime:2018-04-13 23:13:14)
 */
@ApiModel(value = "抽奖表单")
public class DrawWorker {
    @ApiModelProperty(value = "抽奖等级")
    private Integer rank;
    @ApiModelProperty(value = "员工号集合")
    private String[] numbers;

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String[] getNumbers() {
        return numbers;
    }

    public void setNumbers(String[] numbers) {
        this.numbers = numbers;
    }
}

