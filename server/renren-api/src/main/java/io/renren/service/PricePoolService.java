package io.renren.service;

import com.baomidou.mybatisplus.service.IService;
import io.renren.entity.PricePoolEntity;

/**
 * 奖池
 *
 * @author chenshun
 */
public interface PricePoolService extends IService<PricePoolEntity> {

    PricePoolEntity selectByCurrent();

    void updateByRank(int rank);
    int queryPerByRank(int rank);
}

