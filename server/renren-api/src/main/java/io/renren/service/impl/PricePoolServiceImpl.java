package io.renren.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.renren.dao.PricePoolDao;
import io.renren.entity.PricePoolEntity;
import io.renren.service.PricePoolService;
import org.springframework.stereotype.Service;


/**
 * @author zhengheming
 */
@Service("pricePoolService")
public class PricePoolServiceImpl extends ServiceImpl<PricePoolDao, PricePoolEntity> implements PricePoolService {


    @Override
    public PricePoolEntity selectByCurrent() {
        return baseMapper.selectByCurrent();
    }

    @Override
    public void updateByRank(int rank) {
        PricePoolEntity pricePoolEntity = new PricePoolEntity();
        pricePoolEntity.setRank(rank);
        pricePoolEntity = baseMapper.selectOne(pricePoolEntity);

        pricePoolEntity.setCurrentCount(pricePoolEntity.getCurrentCount() + pricePoolEntity.getPer());

        baseMapper.updateById(pricePoolEntity);


    }

    @Override
    public int queryPerByRank(int rank) {
        PricePoolEntity pricePoolEntity = new PricePoolEntity();
        pricePoolEntity.setRank(rank);
        pricePoolEntity = baseMapper.selectOne(pricePoolEntity);
        return pricePoolEntity.getPer();
    }
}
